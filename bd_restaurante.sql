-- Crear la base de datos
CREATE DATABASE restaurante;
USE restaurante;

-- Crear la tabla Clientes
CREATE TABLE Clientes (
    ID_Cliente INT PRIMARY KEY,
    Nombre_Cliente VARCHAR(100) NOT NULL,
    Ciudad VARCHAR(50),
    Pais VARCHAR(50),
    Codigo_Postal VARCHAR(20),
    Codigo_Pais CHAR(2)
);

-- Crear la tabla Personal
CREATE TABLE Personal (
    ID_Personal INT PRIMARY KEY,
    Nombre_Personal VARCHAR(100) NOT NULL,
    Cargo VARCHAR(50),
    Salario DECIMAL(10, 2)
);

-- Crear la tabla Reservas
CREATE TABLE Reservas (
    Numero_Reserva INT PRIMARY KEY,
    ID_Cliente INT,
    ID_Personal INT,
    FOREIGN KEY (ID_Cliente) REFERENCES Clientes(ID_Cliente),
    FOREIGN KEY (ID_Personal) REFERENCES Personal(ID_Personal)
);

-- Crear la tabla Estados_Entrega
CREATE TABLE Estados_Entrega (
    ID_Estado INT PRIMARY KEY,
    Descripcion_Estado VARCHAR(50) NOT NULL
);

-- Crear la tabla Menu
CREATE TABLE Menu (
    ID_Menu INT PRIMARY KEY,
    Nombre_Platillo VARCHAR(100),
    Nombre_Cocina VARCHAR(50),
    Nombre_Entrada VARCHAR(50),
    Nombre_Postre VARCHAR(50),
    Nombre_Bebida VARCHAR(50),
    Nombre_Acompanamiento VARCHAR(50)
);

-- Crear la tabla Pedidos
CREATE TABLE Pedidos (
    ID_Pedido INT PRIMARY KEY,
    Fecha_Pedido DATE,
    Fecha_Entrega DATE,
    ID_Cliente INT,
    Costo DECIMAL(10, 2),
    Ventas DECIMAL(10, 2),
    Cantidad INT,
    Descuento DECIMAL(5, 2),
    Costo_Entrega DECIMAL(10, 2),
    Numero_Reserva INT,
    ID_Estado INT,
    FOREIGN KEY (ID_Cliente) REFERENCES Clientes(ID_Cliente),
    FOREIGN KEY (Numero_Reserva) REFERENCES Reservas(Numero_Reserva),
    FOREIGN KEY (ID_Estado) REFERENCES Estados_Entrega(ID_Estado)
);

-- Crear la tabla PedidosMenu (tabla intermedia para la relación N:M)
CREATE TABLE PedidosMenu (
    ID_Pedido INT,
    ID_Menu INT,
    PRIMARY KEY (ID_Pedido, ID_Menu),
    FOREIGN KEY (ID_Pedido) REFERENCES Pedidos(ID_Pedido),
    FOREIGN KEY (ID_Menu) REFERENCES Menu(ID_Menu)
);

-- Tarea 1 En la primera tarea, Little Lemon necesita que cree una tabla virtual llamada OrdersView
CREATE VIEW VistasPedidos AS
SELECT ID_Pedido, Cantidad, Costo
FROM Pedidos
WHERE Cantidad > 2;

--Tarea 2 En la segunda tarea, Little Lemon necesita información de c
--uatro tablas sobre todos los clientes con pedidos de más de $150.
SELECT
    c.ID_Cliente, c.Nombre_Cliente,
    p.ID_Pedido, p.Costo,
    m.Nombre_Platillo, m.Nombre_Entrada, m.Nombre_Postre, m.Nombre_Bebida, m.Nombre_Acompanamiento
FROM Clientes c
JOIN Pedidos p ON c.ID_Cliente = p.ID_Cliente
JOIN PedidosMenu pm ON p.ID_Pedido = pm.ID_Pedido
JOIN Menu m ON pm.ID_Menu = m.ID_Menu
WHERE p.Costo > 150
ORDER BY p.Costo ASC;

--Tarea 3 En la tercera y última tarea, Little Lemon necesita que 
--busque todos los platos del menú para los que se hicieron más de dos pedidos
SELECT Nombre_Platillo
FROM Menu
WHERE ID_Menu = ANY (
    SELECT ID_Menu
    FROM PedidosMenu
    WHERE ID_Pedido IN (
        SELECT ID_Pedido
        FROM Pedidos
        WHERE Cantidad > 2
    )
);
--Tarea 1 En esta primera tarea, Little Lemon necesita que cree un procedimiento q
--que muestre la cantidad máxima pedida en la tabla Orders (Pedidos).

DELIMITER $$
CREATE PROCEDURE ObtenerCantidadMaximaPedida()
BEGIN
    SELECT MAX(Cantidad) AS CantidadMaxima
    FROM Pedidos;
END$$
DELIMITER ;

--Tarea 2 En la segunda tarea, Little Lemon necesita su ayuda 
--para crear una sentencia preparada llamada GetOrderDetail

-- Crear la sentencia preparada
DELIMITER $$
CREATE PROCEDURE GetOrderDetail(IN customer_id INT)
BEGIN
    SELECT ID_Pedido, Cantidad, Costo
    FROM Pedidos
    WHERE ID_Cliente = customer_id;
END$$
DELIMITER ;

-- Crear una variable y asignar un valor
SET @id = 1;

-- Ejecutar la sentencia preparada
PREPARE stmt FROM 'CALL GetOrderDetail(?)';
EXECUTE stmt USING @id;

-- Liberar la sentencia preparada
DEALLOCATE PREPARE stmt;

-- Tarea 3 Su tercera y última tarea consiste en crear un 
-- procedimiento almacenado llamado CancelOrder. Little Lemon 

DELIMITER $$
CREATE PROCEDURE CancelOrder(IN order_id INT)
BEGIN
    IF EXISTS (SELECT 1 FROM Pedidos WHERE ID_Pedido = order_id) THEN
        DELETE FROM Pedidos WHERE ID_Pedido = order_id;
        SELECT CONCAT('Pedido ', order_id, ' cancelado correctamente.') AS Mensaje;
    ELSE
        SELECT CONCAT('El pedido ', order_id, ' no existe.') AS Mensaje;
    END IF;
END$$
DELIMITER ;

-- Tarea 1 Little Lemon desea completar la tabla Bookings 
--(Reservas) de su base de datos con algunos registros de datos.
INSERT INTO Reservas (Numero_Reserva, ID_Cliente, ID_Personal)
VALUES
    (1, 1, 1), -- Asumiendo que existen los IDs 1 en las tablas Clientes y Personal
    (2, 3, 2), -- Asumiendo que existen los IDs 3 en Clientes y 2 en Personal
    (3, 2, 3), -- Asumiendo que existen los IDs 2 en Clientes y 3 en Personal
    (4, 1, 4); -- Asumiendo que existen los IDs 1 en Clientes y 4 en Personal
	
-- Tarea 2 En su segunda tarea, Little Lemon necesita que cree un 
-- procedimiento almacenado llamado CheckBooking
DELIMITER $$
CREATE PROCEDURE CheckBooking(
    IN booking_date DATE,
    IN table_number INT
)
BEGIN
    DECLARE table_status VARCHAR(20);
    
    SELECT
        CASE
            WHEN EXISTS (
                SELECT 1
                FROM Reservas
                WHERE Fecha_Reserva = booking_date AND Numero_Mesa = table_number
            ) THEN 'Mesa reservada'
            ELSE 'Mesa disponible'
        END INTO table_status;
        
    SELECT table_status AS 'Estado de la mesa';
END$$
DELIMITER ;

-- Tarea 3 En la tercera y última tarea, Little Lemon debe verificar una reserva 
-- y rechazar cualquier reserva de mesas que ya estén reservadas bajo otro nombre. 
DELIMITER $$
CREATE PROCEDURE AgregarReservaValida(
    IN numero_reserva INT,
    IN id_cliente INT,
    IN id_personal INT
)
BEGIN
    DECLARE reserva_existente BOOLEAN DEFAULT FALSE;
    
    START TRANSACTION;
    
    INSERT INTO Reservas (Numero_Reserva, ID_Cliente, ID_Personal)
    VALUES (numero_reserva, id_cliente, id_personal);
    
    SELECT reserva_existente = EXISTS (
        SELECT 1
        FROM Reservas
        WHERE Numero_Reserva = numero_reserva
    ) INTO @reserva_existente;
    
    IF @reserva_existente THEN
        ROLLBACK;
        SELECT 'La reserva ha sido cancelada. Ya existe una reserva con ese número.' AS 'Mensaje';
    ELSE
        COMMIT;
        SELECT 'Reserva agregada exitosamente.' AS 'Mensaje';
    END IF;
    
END$$
DELIMITER ;

-- 
CALL AgregarReservaValida(5, 1, 1);

--Tarea 1 En esta primera tarea debe crear un nuevo procedimiento 
--llamado AddBooking para agregar un nuevo registro de reserva a la tabla

DELIMITER $$
CREATE PROCEDURE AddBooking(
    IN numero_reserva INT,
    IN fecha_reserva DATE,
    IN numero_mesa INT,
    IN id_cliente INT,
    IN id_personal INT
)
BEGIN
    INSERT INTO Reservas (Numero_Reserva, Fecha_Reserva, Numero_Mesa, ID_Cliente, ID_Personal)
    VALUES (numero_reserva, fecha_reserva, numero_mesa, id_cliente, id_personal);

    SELECT CONCAT('Reserva ', numero_reserva, ' agregada exitosamente.') AS Mensaje;
END$$
DELIMITER ;

--Tarea 2 En su segunda tarea, Little Lemon necesita que 
--cree un nuevo procedimiento llamado UpdateBooking 

DELIMITER $$
CREATE PROCEDURE UpdateBooking(
    IN numero_reserva INT,
    IN nueva_fecha_reserva DATE
)
BEGIN
    IF EXISTS (
        SELECT 1
        FROM Reservas
        WHERE Numero_Reserva = numero_reserva
    ) THEN
        UPDATE Reservas
        SET Fecha_Reserva = nueva_fecha_reserva
        WHERE Numero_Reserva = numero_reserva;

        SELECT CONCAT('La reserva ', numero_reserva, ' ha sido actualizada con la nueva fecha ', nueva_fecha_reserva) AS Mensaje;
    ELSE
        SELECT CONCAT('La reserva ', numero_reserva, ' no existe.') AS Mensaje;
    END IF;
END$$
DELIMITER ;

